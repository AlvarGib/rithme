const express = require('express');
const styleController = require('../controllers/style.controller');

const router = express.Router();

router.get("/", styleController.indexGet);

module.exports = router;