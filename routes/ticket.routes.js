const express = require('express');
const ticketController = require('../controllers/ticket.controller');

const router = express.Router();

router.get("/", ticketController.indexGet);

router.post("/create", ticketController.indexPost);

module.exports = router;