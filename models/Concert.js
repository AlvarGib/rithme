const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const concertSchema = new Schema({
    date: { type: String, required: true },
    price: { type: Number, required: true },
    hour: { type: String },
    concertHall: [{ type: mongoose.Types.ObjectId, ref: 'ConcertHall' }],
    artist: [{ type: mongoose.Types.ObjectId, ref: 'Artist' }],
}, {
    timestamps: true,
})

const Concert = mongoose.model('Concert', concertSchema);

module.exports = Concert;