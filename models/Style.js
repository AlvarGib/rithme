const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const styleSchema = new Schema({
    name: { type: String },
    picture: { type: String },
}, {
    timestamps: true,
})

const Style = mongoose.model('Style', styleSchema);

module.exports = Style;