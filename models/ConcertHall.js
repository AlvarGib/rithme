const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const concertHallSchema = new Schema({
    name: { type: String, required: true },
    image: { type: String },
    location: { type: String },
    city: { type: String },
}, {
    timestamps: true,
})

const ConcertHall = mongoose.model('ConcertHall', concertHallSchema);

module.exports = ConcertHall;