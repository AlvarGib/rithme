const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ticketSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    postelcode: { type: Number, required: true },
    tickets: { type: Object },
    singer: { type: Object },
    hall: { type: Object },
    date: { type: String, required: true },
    hour: { type: String, required: true },
    price: { type: Number, required: true },
    user: { type: String, required: true },
}, {
    timestamps: true,
});

const Ticket = mongoose.model("Tickets", ticketSchema);

module.exports = Ticket;