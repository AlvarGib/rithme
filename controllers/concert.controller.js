const Concert = require('../models/Concert');

const indexGet = async(req, res, next) => {
    try {
        const concerts = await Concert.find().populate('artist').populate('concertHall')

        return res.status(200).json({ concerts });
    } catch (err) {
        next(err);
    }
};

const indexGetId = async(req, res, next) => {
    try {
        const { id } = req.params;
        const concert = await Concert.findById(id).populate('artist').populate('concertHall');

        return res.status(200).json({ concert });

    } catch (error) {
        next(error);
    }
};

const indexPost = async(req, res, next) => {
    try {
        const newconcert = new Concert({
            date: req.body.date,
            price: req.body.price,
            hour: req.body.hour,
        });

        const createdConcert = await newconcert.save();
        return res.status(200).json(createdConcert);
    } catch (err) {
        next(err);
    }
};

const indexPut = async(req, res, next) => {
    try {
        const concertId = req.body.concertId;
        const artistId = req.body.artistId;
        const concertHallId = req.body.concertHallId;

        const updatedConcert = await Concert.findByIdAndUpdate(
            concertId, { $push: { artist: artistId, concertHall: concertHallId } }, { new: true }
        );
        return res.status(200).json(updatedConcert);
    } catch (err) {
        next(err);
    }
}

module.exports = { indexGet, indexPost, indexPut, indexGetId };