const mongoose = require('mongoose');
const DB_URL = require('../db').DB_URL;
const Artist = require('../models/Artist');

const artists = [{
        name: "Judith Hill",
        picture: "https://cadenaser00.epimg.net/ser/imagenes/2019/07/18/radio_palencia/1563450074_934242_1563450302_noticia_normal.jpg",
        gender: "Soul",
        description: "Judith Hill es una de las grandes voces del Soul en US habiendo sido alabada por la revista Rolling Stone por su “estelar potencia de voz”. Además de componer y tocar sus propias canciones Judith (quién escribió su 1ª canción con sólo 4 años) ha acompañado artistas como Stevie Wonder, Michael Jackson, George Benson, Chaka Khan ( Ver vídeos).Ella es una de las historias verdaderas que el director Morgan Neville cuenta en su aclamado 20 Feet From Stardom, sobre los cantantes detrás de las grandes estrellas del siglo XXI, película por la que ganó el Grammy 2015 a la mejor película musical.Sus 1er álbum “Back in Time” fue producido por ella misma y Prince para el sello NPG. Sus conciertos abriendo para Josh Groban, John Legend o Prince en US y UK le han valido el reconocimiento del público.",
    },
    {
        name: "Arctic Monkeys",
        picture: "https://www.oaplus.it/wp-content/uploads/2020/12/arctic-monkeys.jpg",
        gender: "Rock",
        description: "Arctic Monkeys es un grupo de Sheffield (Inglaterra) formado en sus orígenes por el guitarrista, cantante y principal compositor Alex Turner (nacido el 6 de enero de 1986), el guitarrista Jamie Cook (nacido el 8 de julio de 1985), el bajista Andy Nicholson y el batería Matt Helders (nacido el 7 de mayo de 1986). Esta formación debutó en disco en el sello Bang Bang Recordings con el EP “Five Minutes With Arctic Monkeys” (2005), en donde incluyeron el tema “Fake Tales Of San Francisco”.",
    },
    {
        name: "Muse",
        picture: "https://www.google.com/url?sa=i&url=http%3A%2F%2Ft0.gstatic.com%2Fimages%3Fq%3Dtbn%3AANd9GcRAW298toQM6vNwq0o5QX642hqgOgVNyoXINl_nO4ZAoIiF8j2c&psig=AOvVaw2dkA3z4cXCEAOd4KBKU2SJ&ust=1613674792297000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKDgtJrN8e4CFQAAAAAdAAAAABAD",
        gender: "Rock",
        description: "La banda musical Muse, se formó en el año 1994, en la ciudad de Teignmoouth, en el Reino Unido. La banda musical tiene un género marcado de rock, aunque en algunos de sus álbumes han desarrollado varios tipos de estilos distintos, como el heavy metal, rock electrónico, rock espacial o rock progresivo entre otros.",
    },
    {
        name: "Vetusta Morla",
        picture: "https://www.cruillabarcelona.com/wp-content/uploads/2018/10/600vetusta.jpg",
        gender: "Indie",
        description: "Vetusta Morla es una banda de rock indie, que se formó durante el verano de 1998 en Tres Cantos, una localidad de Madrid. Sus integrantes se conocieron en el instituto José Luis Sampedro. Decidieron llamarse Vetusta Morla en honor a la tortuga del libro infantil de Michael Ende, La historia interminable.",
    },
    {
        name: "Morrisey",
        picture: "https://media.resources.festicket.com/www/artists/morrissey.jpg",
        gender: "Rock",
        description: "Steven Patrick Morrissey, nacido en Manchester (Inglaterra), es uno de los iconos más enigmáticos de la música británica en las últimas décadas. Dotado de un aura melancólica, lirismo emocional, imaginería poética y tañido sardónico, una posición independiente ante las grandes corporaciones al margen de modas musicales, una mitomanía melómana y cinéfila percibida en sus actividades artísticas, una ambiguedad en su condición sexual, y una genialidad en su afectada expresión vocal, Morrissey lideró junto al guitarrista Johnny Marr uno de los grupos definitivos de los años 80, The Smiths, antes de dar inicio a una meritoria carrera en solitario.",
    },
    {
        name: "Metallica",
        picture: "https://img.discogs.com/AKqBNMTIel3OrWIP0Cy5zkUyBkA=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/A-18839-1599763766-2316.jpeg.jpg",
        gender: "Rock",
        description: "Metallica es una banda de thrash metal originaria de Estados Unidos. Fue fundada en 1981 en los Ángeles por el danés Lars Ulrich y James Hetfield, a los que se les unirían Lloyd Grant y Ron McGovney.  Es considerada como una de las cuatro grandes agrupaciones del sub-género y a lo largo de sus treinta y cinco años de carrera ha logrado 9 Grammys, 2 American Music Awards, 2 premios MTV, 2 premios Billboard y una estrella en el Paseo de la Fama.",
    },
    {
        name: "Love of Lesbian",
        picture: "https://corrientescirculares.es/wp-content/uploads/2017/10/Love-of-Lesbian-1200x1098.jpg",
        gender: "Indie",
        description: "Love of Lesbian es un grupo de pop-rock indie español de Barcelona. Love of lesbian empiezan a tocar en otoño de 1997 formado por Santi Balmes (voz, guitarra y teclados), Jordi Roig (guitarra), Joan Ramon Planell (bajo y sintetizador) y Oriol Bonet (batería y programación). Más tarde se incorporaría a la banda Julián Saldarriaga (guitarra, secuenciadores y coros). Actualmente, esta formación la completa en los directos Dani Ferrer (teclados).",
    },
]

mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async() => {
        const allArtist = await Artist.find();

        if (allArtist.length) {
            await Artist.collection.drop();
        }
    })
    .catch((err) => {
        console.log(`Error deleting db data ${err}`);
    })
    .then(async() => {
        await Artist.insertMany(artists);
    })
    .catch((err) => {
        console.log(`Error adding data to our db ${err}`)
    })
    .finally(() => mongoose.disconnect());